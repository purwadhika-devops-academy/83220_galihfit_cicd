provider "aws" {
  region = "ap-southeast-1"
}

resource "aws_instance" "default" {
  count         = 3
  ami           = var.ami
  instance_type = var.instance_type

  subnet_id              = var.subnet_id
  vpc_security_group_ids = var.security_groups_id

  user_data = <<EOF
    #!/bin/bash
    sudo apt install nginx -y
    sudo systemctl start nginx
  EOF

  tags = var.tags
}
