terraform {
  backend "s3" {
    bucket = "s3-ganis-bucket"
    key    = "aws-tf-workshop/ec2-by-dika"
    region = "ap-southeast-1"
  }
}
