terraform {
  backend "s3" {
    bucket = "s3-ganis-bucket"
    key    = "aws-tf-workshop/elb-gitops"
    region = "ap-southeast-1"
  }
}
