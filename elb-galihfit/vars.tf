variable "tags" {
  type = map(string)
  default = {
    Name      = "nginx-elb"
    ManagedBy = "Terrafrom"
  }
}

variable "subnets" {
  type = list(string)
  default = [
    "subnet-0fb37140a2fbaec9c",
    "subnet-0206187df2aea9e37",
    "subnet-09ca03dc738a6f30b",
  ]
}

variable "security_groups" {
  type = list(string)
  default = [
    "sg-09add3249e9626859"
  ]
}