provider "aws" {
  region = "ap-southeast-1"
}

data "aws_instances" "this" {
  instance_tags = {
    Role = "webserver"
  }

  instance_state_names = ["running"]
}

resource "aws_elb" "this" {
  count              = 1
  name               = "nginx-elb"

  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTP:80/"
    interval            = 30
  }

  instances                   = data.aws_instances.this.ids
  cross_zone_load_balancing   = true
  idle_timeout                = 400
  connection_draining         = true
  connection_draining_timeout = 400

  subnets         = var.subnets
  security_groups = var.security_groups
  tags            = var.tags
}