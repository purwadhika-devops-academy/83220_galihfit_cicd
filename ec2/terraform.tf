terraform {
  backend "s3" {
    bucket = "s3-ganis-bucket"
    key    = "aws-tf-workshop/ec2-gitops"
    region = "ap-southeast-1"
  }
}
