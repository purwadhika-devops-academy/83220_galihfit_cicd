
variable "ami" {
  type    = string
  default = "ami-03ca998611da0fe12"
}

variable "instance_type" {
  type    = string
  default = "t3.micro"
}

variable "tags" {
  type = map(string)
  default = {
    Name      = "nginx"
    Role      = "webserver"
    ManagedBy = "Terrafrom"
  }
}

variable "subnet_id" {
  type    = string
  default = "subnet-0fb37140a2fbaec9c"
}

variable "security_groups_id" {
  type = list(string)
  default = [
    "sg-09add3249e9626859"
  ]
}